; =====================================================================================
; Module ..........: ScintillaWpr (ie. ScintillaPanelsAndDialogsHook) v1.00
; Original Author .: Patryk Szczepankiewicz ( pszczepa at gmail dot com )
; Description .....: Program that handles ENTER and Ctrl+Alt+F keystrokes and
;                    enhances the "Find In Files" tool user experience.
; History .........: JUL 27, 2011 - Released by pszczepa
; =====================================================================================

Opt("MustDeclareVars", 1)

#include <Debug.au3>
Global Const $DEBUG_MODE = 0

#include <Process.au3>
#include <Misc.au3>
#include <String.au3>

#include "Inc.ScintillaWpr.Helpers.au3"

Global Const $DEFAULT_FIND_FILES_PATTERN = "*"
;Global Const $DEFAULT_FIND_FILES_PATTERN = " "

Global Const $HOTKEY_SCI_PANE_SWITCHER = "^!f"

Global Const $FIF_TITLE = "Find in Files"
Global Const $FIF_PROC = "SciTE.exe"

Global Const $SCINTILLA_CLASS = "[CLASS:SciTEWindow]"
Global Const $NOTEPADPP_CLASS = "[CLASS:Notepad++]"
;Global Const $VISUALSTU_CLASS = "[CLASS:HwndWrapper[DefaultDomain;;ac086ed8-21fd-41fb-8656-e0d578aa830d]]" ; main window
;Global Const $VISUALSTU_CLASS = "HwndWrapper[DefaultDomain;;bb38fe9d-dac4-4974-af39-1c64945c8d4f]" ; find in files window

Global Const $SCINTILLA_PANE_1_CLASSNAME_NN = "Scintilla1"
Global Const $SCINTILLA_PANE_2_CLASSNAME_NN = "Scintilla2"

ScintillaWpr_Main()


#region SciTE Find In Files (FIF)

Func SciteFif_GetHandle($timeout = 0)
	Local $wndHandle = WinGetHandle($FIF_TITLE)
	Local $fifWndExists = ($wndHandle <> "") _
			And WinIsVisible($wndHandle) _
			and (_ProcessGetName(WinGetProcess($wndHandle)) = $FIF_PROC)
	If Not $fifWndExists Then $wndHandle = ""
	Return $wndHandle
EndFunc   ;==>SciteFif_GetHandle

Func SciteFif_SearchDirGet($wndHandle)
	Return ControlGetText($wndHandle, "", "Edit3")
EndFunc   ;==>SciteFif_SearchDirGet

Func SciteFif_SearchDirSet($wndHandle, $searchDir)
	Return ControlSetText($wndHandle, "", "Edit3", $searchDir)
EndFunc   ;==>SciteFif_SearchDirSet

Func SciteFif_SearchDirAdd($wndHandle, $searchDir)
	Return ControlCommand($wndHandle, "", "ComboBox3", "AddString", $searchDir)
EndFunc   ;==>SciteFif_SearchDirAdd

Func SciteFif_SearchDirIsChildOf($wndHandle, $searchDir)
	Local $currentSearchDir = SciteFif_SearchDirGet($wndHandle)
	Return SciteFif_SearchDirIsChildOfSub($currentSearchDir, $searchDir)
EndFunc   ;==>SciteFif_SearchDirIsChildOf

Func SciteFif_SearchDirIsChildOfSub($currentSearchDir, $searchDir)
	Local $ret = ($currentSearchDir = $searchDir) Or StringInStr($currentSearchDir, $searchDir & "\")
	Return $ret
EndFunc   ;==>SciteFif_SearchDirIsChildOfSub

Func SciteFif_SearchDirSetOrAdd($wndHandle, $searchDir, $subProjDir = "")
	Local $currentSearchDir = SciteFif_SearchDirGet($wndHandle)
	If SciteFif_SearchDirIsChildOfSub($currentSearchDir, $searchDir) _
			and ($subProjDir = "" or (Not SciteFif_SearchDirIsChildOfSub($currentSearchDir, $subProjDir))) _
			Then
		SciteFif_SearchDirSet($wndHandle, $searchDir)
	Else
		SciteFif_SearchDirAdd($wndHandle, $searchDir)
	EndIf
EndFunc   ;==>SciteFif_SearchDirSetOrAdd

Func SciteFif_SearchExtGet($wndHandle)
	Return ControlGetText($wndHandle, "", "Edit2")
EndFunc   ;==>SciteFif_SearchExtGet

Func SciteFif_SearchExtSet($wndHandle, $searchExt)
	Return ControlSetText($wndHandle, "", "Edit2", $searchExt)
EndFunc   ;==>SciteFif_SearchExtSet

Func SciteFif_SearchExtAdd($wndHandle, $searchExt)
	Return ControlCommand($wndHandle, "", "ComboBox2", "AddString", $searchExt)
EndFunc   ;==>SciteFif_SearchExtAdd

#endregion SciTE Find In Files (FIF)


#region Scintilla Wrapper

; =====================================================================================
; Function ........: ScintillaWpr_Main v1.00
; Description .....: Main function
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ScintillaWpr_Main()

	; WARNING: ; MUTEX USED TO BE "ScintillaWpr"
	_Singleton("SCINTILLA-WRAPPER-48021ABB-CE58-4305-AB01-222B15706ED8")
	TraySetIcon("Shell32.dll", 283) ; keyboard icon
	If StringInStr($CmdLineRaw, "--notray") Then TraySetState(2) ; hide

	If $DEBUG_MODE Then _DebugSetup(@ScriptName)

	HotKeySet($HOTKEY_SCI_PANE_SWITCHER, "ScintillaWpr_HotKeyCallbackForPaneSwitch")
	HotKeySet("{ENTER}", "ScintillaWpr_HotKeyCallbackForEnterKey")

	ScintillaWpr_InfiniteLoopV2()

EndFunc   ;==>ScintillaWpr_Main

; =====================================================================================
; Function ........: ScintillaWpr_FifUiConfig v1.00
; Description .....:
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ScintillaWpr_FifUiConfig($fifWnd)

	Local $iniFile = @ScriptDir & "\" & StringReplace(@ScriptName, ".au3", ".ini")

	Local $allWorkTypes = _StringExplode(IniRead($iniFile, "FindInFiles/EditingWorkTypes", "(Default)", ""), ";")
	ReDim $allWorkTypes[UBound($allWorkTypes) + 1]
	$allWorkTypes[UBound($allWorkTypes) - 1] = "(Default)" ; << Add the default work-type

	Local $allProjDirs = _StringExplode(IniRead($iniFile, "FindInFiles/PopulateCombos/DirPaths/Add", "(Default)", "C:\"), ";")
	Local $allFileTypes = _StringExplode(IniRead($iniFile, "FindInFiles/PopulateCombos/FilePatterns/Add", "(Default)", "*au3"), ";")

	; Add file types
	For $fileType In $allFileTypes
		If $fileType = "" Then ContinueLoop
		SciteFif_SearchExtAdd($fifWnd, $fileType)
	Next

	; Check what kind of project we are working on
	Local $foundType = "Dflt"
	Local $foundRoot = ""
	For $workType In $allWorkTypes
		Local $allDirRoots = _StringExplode(IniRead($iniFile, "FindInFiles/PopulateCombos/DirPaths/Sel", $workType, ""), ";")
		For $dirRoot In $allDirRoots
			If $dirRoot = "" Then ContinueLoop
			Local $isChildOf = SciteFif_SearchDirIsChildOf($fifWnd, StringReplaceAutoITMacros($dirRoot))
			If $DEBUG_MODE Then _DebugOut("SciteFif_SearchDirIsChildOf($fifWnd, " & StringReplaceAutoITMacros($dirRoot) & ")")
			If $DEBUG_MODE Then _DebugOut("$isChildOf = " & $isChildOf)
			If $isChildOf Then
				$foundType = $workType
				$foundRoot = $dirRoot
				ExitLoop 2
			EndIf
		Next
	Next

	; Select the right file extension
	If $foundType <> "" Then
		Local $selExt = IniRead($iniFile, "FindInFiles/PopulateCombos/FilePatterns/Sel", $foundType, $DEFAULT_FIND_FILES_PATTERN)
		SciteFif_SearchExtSet($fifWnd, $selExt)
	EndIf

	; Select the project's root directory
	If $foundRoot <> "" Then
		SciteFif_SearchDirSet($fifWnd, StringReplaceAutoITMacros($foundRoot))
	EndIf

	; All directories
	For $projDir In $allProjDirs
		If $projDir = "" Then ContinueLoop
		SciteFif_SearchDirAdd($fifWnd, StringReplaceAutoITMacros($projDir))
	Next

EndFunc   ;==>ScintillaWpr_FifUiConfig

; =====================================================================================
; Function ........: ScintillaWpr_InfiniteLoopV2 v1.00
; Description .....: Main function
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ScintillaWpr_InfiniteLoopV2()

	Local $fifWnd_ = SciteFif_GetHandle()
	While 1
		Sleep(350)
		Local $fifWnd = SciteFif_GetHandle()
		If $fifWnd <> "" And $fifWnd_ = "" Then
			ScintillaWpr_FifUiConfig($fifWnd)
		EndIf
		$fifWnd_ = $fifWnd
	WEnd

EndFunc   ;==>ScintillaWpr_InfiniteLoopV2

; =====================================================================================
; Function ........: ScintillaWpr_HotKeyCallbackForPaneSwitch v1.00
; Description .....: This is a callback for HotKeySet
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ScintillaWpr_HotKeyCallbackForPaneSwitch()
	If Not ScintillaWpr_PaneSwitch() Then
		ResetAndForwardHotKey(@HotKeyPressed, "ScintillaWpr_HotKeyCallbackForPaneSwitch")
	EndIf
EndFunc   ;==>ScintillaWpr_HotKeyCallbackForPaneSwitch

; =====================================================================================
; Function ........: ScintillaWpr_PaneSwitch v1.00
; Description .....: Switch between the two scintilla panels
;                    (document editor VS. console output)
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ScintillaWpr_PaneSwitch()
	Local $ret = 0, $scintillaWindow = ""
	Local $focusControl
	; Do we use a scintilla editor? And which one?
	If WinActive($NOTEPADPP_CLASS) Then
		$scintillaWindow = $NOTEPADPP_CLASS
	ElseIf WinActive($SCINTILLA_CLASS) Then
		$scintillaWindow = $SCINTILLA_CLASS
	EndIf
	; If yes then we switch the focus between the two usual panels.
	If $scintillaWindow <> "" Then
		Local $focusControl = $SCINTILLA_PANE_1_CLASSNAME_NN
		If ControlGetFocus($scintillaWindow) = $focusControl Then
			$focusControl = $SCINTILLA_PANE_2_CLASSNAME_NN
		EndIf
		ControlFocus($scintillaWindow, "", $focusControl)
		$ret = 1
	EndIf
	Return $ret
EndFunc   ;==>ScintillaWpr_PaneSwitch

; =====================================================================================
; Function ........: ScintillaWpr_HotKeyCallbackForEnterKey v1.00
; Description .....: Callback for HotKeySet
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ScintillaWpr_HotKeyCallbackForEnterKey()
	If Not ScintillaWpr_HandleEnterKey() Then
		ResetAndForwardHotKey(@HotKeyPressed, "ScintillaWpr_HotKeyCallbackForEnterKey")
	EndIf
EndFunc   ;==>ScintillaWpr_HotKeyCallbackForEnterKey

; =====================================================================================
; Function ........: ScintillaWpr_HandleEnterKey v1.00
; Description .....: Called upon Enter key press. Launch some automation
;                    if the active window is a scintilla panel
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ScintillaWpr_HandleEnterKey()
	Local $coord
	If WinActive($SCINTILLA_CLASS) and (ControlGetFocus($SCINTILLA_CLASS) = $SCINTILLA_PANE_2_CLASSNAME_NN) Then
		$coord = WinGetCaretPos()
		; WARNING: Debugging the top left corner of the panel gave: $coord = {8; 42}
		ControlClickUsingMouse($SCINTILLA_CLASS, "", $SCINTILLA_PANE_2_CLASSNAME_NN, "left", 2, $coord[0] + 5, $coord[1])
		; Give the focus back to the main scintilla window
		ControlFocus($SCINTILLA_CLASS, "", $SCINTILLA_PANE_1_CLASSNAME_NN)
		Return True
	Else
		Return False
	EndIf
EndFunc   ;==>ScintillaWpr_HandleEnterKey

#endregion Scintilla Wrapper
