#include-once
; =====================================================================================
; Module ..........: Inc.ScintillaWpr.Helpers.au3 v1.00
; Original Author .: Patryk Szczepankiewicz ( pszczepa at gmail dot com )
; Description .....: Program that handles ENTER and Ctrl+Alt+F keystrokes and
;                    enhances the "Find In Files" tool user experience.
; History .........: JUL 27, 2011 - Released by pszczepa
; =====================================================================================
Opt("MustDeclareVars", 1)


; PROGRAM_FILES_DIR_32 SNIPPET -----------------------------
If Not IsDeclared("PROGRAM_FILES_DIR_32") Then
	Assign("PROGRAM_FILES_DIR_32", @ProgramFilesDir, 2)
	If @AutoItX64 Then Assign("PROGRAM_FILES_DIR_32", @ProgramFilesDir & " (x86)", 2)
EndIf
; -----------------------------------------------------------


Func WinIsVisible($title, $text = "")
	If BitAND(WinGetState($title, $text), 2) Then
		Return True
	Else
		Return False
	EndIf
EndFunc   ;==>WinIsVisible


Func StringReplaceAutoITMacros($string)
	$string = StringReplace($string, "@AppDataDir", @AppDataDir)
	$string = StringReplace($string, "@DesktopDir", @DesktopDir)
	$string = StringReplace($string, "@UserProfileDir", @UserProfileDir)
	$string = StringReplace($string, "@ProgramFilesDir", @ProgramFilesDir)
	$string = StringReplace($string, "@ProgramFilesDir32", Eval("PROGRAM_FILES_DIR_32"))
	$string = StringReplace($string, "@ProgramFilesDir64", StringReplace(@ProgramFilesDir, " (x86)", ""))
	Return $string
EndFunc   ;==>StringReplaceAutoITMacros


; =====================================================================================
; Function ........: ControlClickUsingMouse v1.00
; Description .....: Click inside of a control by moving the mouse very quick and moving
;                    the mouse back to its original position at the end of the operation
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ControlClickUsingMouse($title, $text, $controlID, $button = "left", $clicks = 1, $x = Default, $y = Default)
	Local $mouseCoordModeOpt = 1, $cursor, $control
	; Relative coords to the active window (default = absolute screen coordinates)
	If AutoItSetOption('MouseCoordMode') <> $mouseCoordModeOpt Then
		$mouseCoordModeOpt = AutoItSetOption('MouseCoordMode', 1)
	EndIf
	; Remember original position of the mouse
	$cursor = MouseGetPos()
	; Get position of the control
	$control = ControlGetPos($title, $text, $controlID)
	; Set default values
	If $x = Default Then $x = $control[0] / 2
	If $y = Default Then $y = $control[1] / 2
	; Update to global coordinates
	$x += $control[0]
	$y += $control[1]
	; Perfom the click by using the mouse
	MouseClick($button, $x, $y, $clicks, 1)
	MouseMove($cursor[0], $cursor[1], 1)
	If AutoItSetOption('MouseCoordMode') <> $mouseCoordModeOpt Then
		AutoItSetOption('MouseCoordMode', $mouseCoordModeOpt)
	EndIf
EndFunc   ;==>ControlClickUsingMouse

; =====================================================================================
; Function ........: ResetAndForwardHotKey v1.00
; Description .....: Helper for manipulating hotkeys
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func ResetAndForwardHotKey($forward, $callback)
	HotKeySet($forward)
	Send(TranslateDownUps($forward))
	HotKeySet($forward, $callback)
EndFunc   ;==>ResetAndForwardHotKey

; =====================================================================================
; Function ........: TranslateDownUps v1.00
; Description .....: Helper for manipulating hotkeys
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func TranslateDownUps($strokes)
	Local $prefix = "", $suffix = ""
	TranslateDownUpsSub($strokes, $prefix, $suffix)
	Return $prefix & $strokes & $suffix
EndFunc   ;==>TranslateDownUps

Func TranslateUpsOnly($strokes)
	Local $prefix = "", $suffix = ""
	TranslateDownUpsSub($strokes, $prefix, $suffix)
	Return $suffix
EndFunc   ;==>TranslateUpsOnly

Func SendUpsOnlyIfAny($strokes)
	Local $suffix = TranslateUpsOnly($strokes)
	If $suffix <> "" Then Send($suffix)
EndFunc   ;==>SendUpsOnlyIfAny

; =====================================================================================
; Function ........: TranslateDownUpsSub v1.00
; Description .....: Helper for manipulating hotkeys
; History .........: JUL 27, 2011 - Released by pszczepa for ScintillaWpr.au3
; =====================================================================================
Func TranslateDownUpsSub(ByRef $strokes, ByRef $prefix, ByRef $suffix)
	Local $loop
	While 1
		If StringStartsWith($strokes, "!") Then
			$strokes = StringTrimLeft($strokes, 1)
			$prefix = $prefix & "{ALTDOWN}"
			$suffix = "{ALTUP}" & $suffix
			ContinueLoop
		EndIf
		If StringStartsWith($strokes, "^") Then
			$strokes = StringTrimLeft($strokes, 1)
			$prefix = $prefix & "{CTRLDOWN}"
			$suffix = "{CTRLUP}" & $suffix
			ContinueLoop
		EndIf
		If StringStartsWith($strokes, "+") Then
			$strokes = StringTrimLeft($strokes, 1)
			$prefix = $prefix & "{SHIFTDOWN}"
			$suffix = "{SHIFTUP}" & $suffix
			$loop = 1
			ContinueLoop
		EndIf
		ExitLoop
	WEnd
EndFunc   ;==>TranslateDownUpsSub

;-------------------------------------------------------------------------------
; Name .........: StringStartsWith
; Description ..: not case sensitive by default
; Return .......:
;-------------------------------------------------------------------------------
Func StringStartsWith($string, $begin, $casesense = 0)
	Return Not StringCompare(StringLeft($string, StringLen($begin)), $begin, $casesense)
EndFunc   ;==>StringStartsWith
