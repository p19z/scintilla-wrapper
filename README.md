![Scintilla Wrapper Logo](https://bitbucket.org/deskobj/scintilla-wrapper/raw/master/Res/Banner/scite-wpr-banner.png)



# Ctrl+Alt+F Background #

I have a VC++ developer background and the keyboard shortcuts I use the most are **Ctrl+Shift+F** (Find-in-Files) and... **Ctrl+Alt+F** (Switch focus between the code editor and the search results). Also, once the focus is in the search results pane, I am always tempted to hit **ENTER** instead of clicking on the file/line that I wanted to open. Finally, I find it useful to have the Find-in-file dialog populated with my favourite settings ( files patterns and project directories to search ).

All this is not supported by Scintilla ( whether in SciTE or in Notepad++ ). So I wrote the following program that I run on startup and that makes Scintilla behave the way I want it to.

This is fairly well tested, since I used this for a couple years (XP, Windows 7, x86 and x64) and works on both Notepad++ and SciTE.



# Further developments #

The next problem was the content of the Find in Files dialog itself, so I added a check and populate this dialog as suites as soon as it shows up. This is what the .ini file is for.



# Functional description #

1/ Set a hook on Ctrl+Alt+F and on the ENTER key
+ process keystroke if a Scintilla application has the focus (forward otherwise)

![Scintilla Wrapper Logo](https://bitbucket.org/deskobj/scintilla-wrapper/raw/master/Help/ScintillaWpr%20for%20PaneSwitch%20feature.PNG)

2/ Loop and check every 350ms if the "Find in Files" dialog did not show up
+ populate the combo-boxes according to the configuration file.

![Scintilla Wrapper Logo](https://bitbucket.org/deskobj/scintilla-wrapper/raw/master/Help/ScintillaWpr%20for%20FindInFiles%20feature.PNG)



# Usage #

- Keep the 3 files ScintillaWpr.au3, Inc.ScintillaWpr.Helpers.au3 and ScintillaWpr.ini together (in the same directory)
- Edit the ScintillaWpr.ini to suit your needs (I provided a sample usage)
- Run the ScintillaWpr.au3 script.



# Additional notes #

Do not get into any conflict with any other "findstr" program that might come with Cygwin, Git, or GnuWin32 or god knows what other Unix emulation system. I recommend editting the SciTEGlobal.properties file and setting:
```
#!properties
find.command=C:\WINDOWS\System32\findstr.exe /n /s /i /c:"$(find.what)" $(find.files)
```

Also, the scintilla-wrapper functionality could also be achieved by implementing custom LUA scripts and changing configuration of SciTE.



# Questions and comments #

If you have any questions, please visit:

http://www.autoitscript.com/forum/topic/131261-keyboard-shortcuts-for-scintilla-find-in-files-and-switch-pane-features/

Take care,

P.
